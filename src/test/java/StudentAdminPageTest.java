import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

import static org.testng.Assert.*;

public class StudentAdminPageTest {

    StudentAdminPage studentpage;
    WebDriver driver;

    @BeforeMethod
    public void setUp() throws Exception {
        System.setProperty("webdriver.chrome.driver", "C:\\Program Files\\Java\\Selenium\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        new MainAdminPage(driver).login();
        studentpage = new StudentAdminPage(driver);

    }

    @AfterMethod
    public void tearDown() throws Exception {
        driver.quit();
    }

    @Test
    public void createNewStudent() throws InterruptedException {
        studentpage.clickAddUserButton();
        studentpage.fillGroupId();
        studentpage.fillName();
        studentpage.fillLastName();
        studentpage.fillCvUrlField();
        studentpage.fillImageUrlField();
        studentpage.fillEntryScoreField();
        studentpage.fillApprovedByField();
        Thread.sleep(5000);
        studentpage.clickSubmitButton();
    }

    @Test
    public void validateNewUser() throws InterruptedException {
//        ArrayList<String> studentsExpected = new ArrayList<String>();
//        studentsExpected.add("");
//        studentsExpected.add("12345");
//        studentsExpected.add("Alex");
//        studentsExpected.add("Pass");
//        studentsExpected.add("Elementary");
//        studentsExpected.add("google.com");
//        studentsExpected.add("booking.com");
//        studentsExpected.add("200");
//        studentsExpected.add("Ivanov.I");

        List <WebElement> index1  = driver.findElements(By.cssSelector("#students table tr"));
        List <WebElement> index2  = driver.findElements(By.cssSelector("#students table td"));


        ArrayList<String> studentsActual = new ArrayList<String>();
        for (int i = 2; i < index1.size(); i++) {

            for (int j = 1; j < index2.size(); j++) {

                studentsActual.add(driver.findElement(By.xpath("//*[@id=\"students\"]/div/table/tbody/tr["+ i + "]/td[" + j + "]")).getText());
            }
        }
        for (String students: studentsActual) {
            System.out.println(students);


        }
 }


    @Test
    public void clickAddUserButton() {

        studentpage.getAddStudentButton();
    }

    @Test
    public void testClickEditButton() throws Exception {
        studentpage.clickEditButton();

    }

    @Test
    public void testFillGroupId() throws Exception {
        studentpage.clickAddUserButton();
        studentpage.fillGroupId();
    }

    @Test
    public void testFillName() throws Exception {
        studentpage.clickAddUserButton();
        studentpage.fillName();
    }

    @Test
    public void testFillLastName() throws Exception {
        studentpage.clickAddUserButton();
        studentpage.fillLastName();
    }

    @Test
    public void testFillCvUrl() throws Exception {
        studentpage.clickAddUserButton();
        studentpage.fillCvUrlField();
    }

    @Test
    public void testFillImageUrlField() throws Exception {
        studentpage.clickAddUserButton();
        studentpage.fillImageUrlField();
    }

    @Test
    public void testFillEntryScoreField() throws Exception {
        studentpage.clickAddUserButton();
        studentpage.fillEntryScoreField();
    }

    @Test
    public void testFillApprovedByField() throws Exception {
        studentpage.clickAddUserButton();
        studentpage.fillApprovedByField();
    }


}