import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class StudentAdminPage extends MainAdminPage {
    public static WebDriver driver;

    List<WebElement> editButton;
    WebElement addStudentButton;
    List<WebElement> deleteButton;
    List<WebElement> escapeToHomeButton;
    WebElement groupIdField;
    WebElement nameField;
    WebElement lastName;
    WebElement englishLevel;
    WebElement CvUrlField;
    WebElement imageUrlField;
    WebElement entryScoreField;
    WebElement approvedByField;

    public StudentAdminPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
        new WebDriverWait(driver, 100);
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.get("http://localhost:3000/admin");
        driver.findElement(By.partialLinkText("students")).click();
    }

    public List<WebElement> getEditButton() {
        return driver.findElements(By.cssSelector(".btn.btn-info.edit"));

    }

    public WebElement getAddStudentButton() {
        return driver.findElement(By.cssSelector("#students > div:nth-child(1) > button:nth-child(2)"));
    }

    public List<WebElement> getDeleteButton() {
        return driver.findElements(By.className("btn btn-danger delete"));
    }

    public List<WebElement> getEscapeToHomeButton() {
        return driver.findElements(By.className("btn btn-warning home"));
    }

    public WebElement getGroupIdField() {
        return driver.findElement(By.name("12345"));
    }

    public WebElement getNameField() {
        return driver.findElement(By.name("name"));
    }

    public WebElement getLastNameField() {
        return driver.findElement(By.name("lastName"));
    }

    public WebElement getcvUrlField() {
        return driver.findElement(By.name("CvUrl"));
    }

    public WebElement getImageUrlField() {
        return driver.findElement(By.name("imageUrl"));
    }

    public WebElement getEntryScoreField() {
        return driver.findElement(By.name("entryScore"));
    }

    public WebElement getApprovedByField() {
        return driver.findElement(By.name("approvedBy"));
    }

    public WebElement getSubmitButton() {
        return driver.findElement(By.cssSelector(".btn.btn-primary.submit"));
    }

    //Logic methods

    public void clickEditButton() {
        Random random = new Random();
//        int randomProduct = random.nextInt(getEditButton().size());
//        getEditButton().get(randomProduct).click();
        //       System.out.println( editButton.get(randomProduct));
        for (int i = 0; i < getEditButton().size() - 1; i++) {
            getEditButton().get(random.nextInt(getEditButton().size())).click();
        }

    }

    public void clickAddUserButton() {

        getAddStudentButton().click();

    }

    public void fillGroupId() {
        getGroupIdField().sendKeys("12345");
    }

    public void fillName() {
        getNameField().sendKeys("Alex");
    }

    public void fillLastName() {
        getLastNameField().sendKeys("Pass");
    }

    public void fillCvUrlField() {
        getcvUrlField().sendKeys("google.com");
    }

    public void fillImageUrlField() {
        getImageUrlField().sendKeys("booking.com");
    }

    public void fillEntryScoreField() {
        getEntryScoreField().sendKeys("200");
    }

    public void fillApprovedByField() {
        getApprovedByField().sendKeys("Ivanov.I");
    }

    public void clickSubmitButton() {
        getSubmitButton().click();
    }
}

