//package caesar;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class MainAdminPage {

    public WebDriver driver = null;


    WebElement mainLogoTitle;
    WebElement usersTab;
    WebElement groupsTab;
    WebElement studentsTab;
    WebElement homeButton;


    public MainAdminPage(WebDriver driver) {
        this.driver = driver;
//        login();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.get("http://localhost:3000/admin");
    }

    private void init(WebDriver driver) {


    }

    public WebElement getMainLogoTitle() {

        return driver.findElement(By.xpath("/html/body/div[1]/h2"));
    }

    public WebElement getUsersTab() {
        return driver.findElement(By.partialLinkText("users"));
    }

    public WebElement getGroupsTab() {
        return driver.findElement(By.partialLinkText("group"));
    }

    public WebElement getStudentsTab() {
        return driver.findElement(By.partialLinkText("students"));
    }

    public WebElement getHomeButton() {
        return driver.findElement(By.xpath("/html/body/button"));
    }

    public void login(){
        driver.findElement(By.name("login")).sendKeys("dmytro");
        driver.findElement(By.name("password")).sendKeys("1234");
        driver.findElement(By.cssSelector(".submit")).click();
    }

    public void  usersTabClick(){
        getUsersTab().click();
    }

    public void  groupsTabClick(){
        getGroupsTab().click();
    }

    public void  studentsTabClick(){
        getStudentsTab().click();
    }

    public void  homeButtonbClick(){
        getHomeButton().click();
    }

}